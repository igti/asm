export const formatValuePercent = value => {
  return value + '%';
};

export const formatValueMoney = value => {
  return 'R$ ' + formatValue(value);
};

const formatValue = value => {
  if (isWholeNumber(value)) {
    return value + ',00';
  }

  let newValue = value.toFixed(2);
  const whole = newValue.split('.')[0];
  let decimal = newValue.split('.')[1];
  decimal = leftPad(decimal, 2, '0');

  return whole + ',' + decimal;
};

const leftPad = (value, count, char) => {
  if (value.length >= count) {
    return value;
  }

  let newValue = value;

  for (let i = 0; i < count - value.length; i++) {
    newValue = char + newValue;
  }

  return newValue;
};

const isWholeNumber = number => {
  return number % 1 === 0;
};
