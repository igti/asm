import React from 'react';
import { StyleSheet, View, Text, Slider } from 'react-native';

import { formatValueMoney, formatValuePercent } from '../helpers/FormatHelpers';

export default (TransportApp = props => {
  const { app, handleChangeValue } = props;

  let containerStyle = app.winner
    ? { ...styles.winner, ...styles.container }
    : styles.container;

  return (
    <View style={containerStyle}>
      <Text style={styles.title}>
        {app.name} - {formatValueMoney(app.finalPrice)}
      </Text>

      <Text style={styles.subtitle}>
        Percentual de desconto ({formatValuePercent(app.discountPercentage)})
      </Text>
      <View style={styles.sliderView}>
        <Text>{0}</Text>
        <Slider
          style={{ flex: 1 }}
          minimumValue={0}
          maximumValue={50}
          onValueChange={newValue =>
            handleChangeValue('discountPercentage', newValue)
          }
          value={app.discountPercentage}
          step={1}
        />
        <Text>{50}</Text>
      </View>

      <Text style={styles.subtitle}>
        Desconto máximo ({formatValueMoney(app.maximumDiscount)})
      </Text>
      <View style={styles.sliderView}>
        <Text>{5}</Text>
        <Slider
          style={{ flex: 1 }}
          minimumValue={5}
          maximumValue={100}
          onValueChange={newValue =>
            handleChangeValue('maximumDiscount', newValue)
          }
          value={app.maximumDiscount}
          step={1}
        />
        <Text>{100}</Text>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    padding: 20,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'black',
    borderRadius: 10,
    marginVertical: 10
  },

  title: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10
  },

  subtitle: {
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 10
  },

  sliderView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20
  },

  winner: {
    backgroundColor: '#b8e994'
  }
});
