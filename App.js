import React, { Component } from 'react';
import { StyleSheet, Text, View, Slider, ScrollView } from 'react-native';

import { formatValueMoney } from './src/helpers/FormatHelpers';
import TransportApp from './src/components/TransportApp';

/**
 * Fonte de dados
 */
const transportationApps = [
  {
    name: 'Ebur',
    discountPercentage: 20,
    maximumDiscount: 10,
    finalPrice: 0,
    winner: false
  },

  {
    name: '33 TOT',
    discountPercentage: 10,
    maximumDiscount: 20,
    finalPrice: 0,
    winner: false
  }
];

/**
 * Classes em React geralmente extendem
 * de Component
 */
export default class App extends Component {
  /**
   * Construtor da classe
   */
  constructor() {
    /**
     * É obrigatório invocar
     * o construtor da classe pai
     */
    super();

    /**
     * Definindo o estado da aplicação
     */
    this.state = {
      averagePrice: 10,
      apps: transportationApps
    };
  }

  /**
   * Ciclo de vida do React que é
   * invocado quando o componente
   * termina de ser montado em tela
   */
  componentDidMount() {
    this.calculate();
  }

  /**
   * Método principal da classe, que
   * realiza o cálculo propriamente dito
   */
  calculate = () => {
    const { apps, averagePrice } = this.state;

    /**
     * Para cada item da fonte de dados,
     * obtemos o desconto e o valor final,
     * conforme as regras
     */
    apps.forEach(app => {
      const discount = 1 - app.discountPercentage / 100;

      app.finalPrice = Math.max(
        averagePrice - app.maximumDiscount,
        averagePrice * discount
      );

      app.winner = false;
    });

    /**
     * Verificando quem tem o preço mais barato
     */
    let cheapestPrice = 99999999;
    let cheapestPriceIndex = 0;

    apps.forEach((app, index) => {
      if (app.finalPrice < cheapestPrice) {
        cheapestPrice = app.finalPrice;
        cheapestPriceIndex = index;
      }
    });

    /**
     * Definindo o "vencedor"
     */
    apps[cheapestPriceIndex].winner = true;

    /**
     * Atualizando o estado da aplicação
     */
    this.setState({ apps });
  };

  /**
   * Método para trocar o valor do array 'apps'
   * de forma genérica, ou seja, o campo a ser
   * alterado é passado como parâmetro
   */
  changeValue = (field, newValue, index) => {
    const newApps = this.state.apps;

    /**
     * Aqui, sabemos o índice do vetor e
     * qual campo deve ser alterado. A utilização
     * de colchetes é uma forma alternativa de
     * se modificar objetos JSON
     */
    newApps[index][field] = newValue;

    /**
     * Após a mudança de estado,
     * invocamos o método de
     * cálculo novamente
     */
    this.setState(
      {
        apps: newApps
      },
      this.calculate()
    );
  };

  /**
   * Método de renderização
   * do componente, que toda
   * classe React deve ter
   */
  render() {
    /**
     * Object destructuring (ES6+)
     */
    const { averagePrice, apps } = this.state;

    /**
     * Código JSX
     */
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.title}>Comparativo</Text>
        <Text style={styles.subtitle}>
          Preço médio da corrida: {formatValueMoney(averagePrice)}
        </Text>

        <View style={styles.sliderView}>
          <Text>10</Text>
          <Slider
            style={{ flex: 1 }}
            minimumValue={10}
            maximumValue={200}
            onValueChange={newValue => {
              this.setState({ averagePrice: newValue }, this.calculate());
            }}
            value={averagePrice}
            step={1}
          />
          <Text>200</Text>
        </View>

        <View>
          {apps.map((app, index) => {
            return (
              <TransportApp
                key={app.name}
                app={app}
                handleChangeValue={(field, newValue) =>
                  this.changeValue(field, newValue, index)
                }
              />
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

/**
 * Objeto de estilos
 * para o App (CssInJs)
 */
const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: 'white'
  },

  title: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10
  },

  subtitle: {
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 10
  },

  sliderView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  }
});
